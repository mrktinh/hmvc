<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Route;
use Illuminate\Support\Facades\Auth;


class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $current_route = Route::currentRouteName();
        // //$level = Auth::user()->level;
        // $role_id = Auth::user()->role_id;
        // $roles = DB::table('roles')->select('value')->where('id', $role_id)->first();
        // $role_array = explode(',', $roles->value);
        // if(in_array($current_route, $role_array)) {
            return $next($request);
        // } else {
        //     return back()->with(['flash_level'=> 'warning','flash_message' => 'Can not access this area!']);
        // }
    }
}
