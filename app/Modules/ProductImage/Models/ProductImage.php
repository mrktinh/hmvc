<?php

namespace App\Modules\ProductImage\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $fillable = ['image', 'product_id'];
    public $timestamps = false;

    public function product() {
    	return $this->belongsTo('App\Modules\Product\Models\Product', 'product_id');
    }
}
