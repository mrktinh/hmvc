@extends('admin.master')
@section('controller', 'Role')
@section('action', 'List')
@section('content')

<!-- Page Content -->
<a href="{{ route('Role.getAdd') }}" class="btn btn-primary" style="margin-left:40%">Add new role</a>
<table class="table table-striped table-bordered table-hover" id="RoleListTable">
    <thead>
        <tr align="center">
            <th>ID</th>
            <th>Name</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
            @foreach( $roles as $index => $role)
            <tr class="odd gradeX" align="center">
                <td><a href="{{ route('Role.getEdit', $role->id) }}">{{ $index + 1 }}</a></td>
                <td><a href="{{ route('Role.getEdit', $role->id) }}">{{ $role->name }}</a></td>
                <td class="center">
                    <i class="fa fa-trash-o  fa-fw"></i>
                    <a 
                        onclick=" 
                        return xacnhanxoa(' Bạn có chắc muốn xóa {{ $role->name }} không?');" 
                        href="{{ route('Role.getDelete', $role->id) }}">
                         Delete
                    </a>
                </td>
                <td class="center">
                    <i class="fa fa-pencil fa-fw"></i> 
                    <a href="{{ route('Role.getEdit', $role->id) }}">
                        Edit
                    </a>
                </td>
            </tr>
            @endforeach
    </tbody>
</table>
        
@endsection

    