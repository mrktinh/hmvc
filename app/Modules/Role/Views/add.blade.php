@extends('admin.master')
@section('controller', 'Role')
@section('action', 'add')
@section('content')

<form action="{{ route('Role.postAdd') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div>
        <div class="row" style="margin: 0px 0px 15px 15px">
            <div class="col-md-2 btn btn-success">Name of the Role</div>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control" value="" name="nameRole" placeholder="Enter the name">
            @if($errors->has('nameRole'))
                <p style="color:red">{{ $errors->first('nameRole') }}</p>
            @endif
        </div>
        <div class="col-md-10" style="margin-bottom:100px">
            @foreach ($routes as $index => $route)
                @if( count(explode('/', $route->path)) == 1 )
                    @if($index != 0) 
                    </div>
                    @endif
                    <div>
                        <div class="btn btn-success" style="display:block;width:100px;margin-top:15px; margin-bottom:10px">{{ $route->path }}</div>
                        <label class="checkbox-inline"><input type="checkbox" name="route[]" value="{{ $route->name }}">{{ $route->function }}</label>
                @else 
                    <label class="checkbox-inline"><input type="checkbox" name="route[]" value="{{ $route->name }}">{{ $route->function }}</label>
                @endif
            @endforeach
        </div>
        <div class="row" style="margin-top:10px;">
            <input type="submit" class="col-md-1 col-md-offset-5 btn btn-primary" value="Add" />
        </div>
    </div>
</form>



@endsection