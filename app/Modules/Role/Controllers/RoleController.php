<?php
namespace App\Modules\Role\Controllers;

use Validator;
use Route;
use DB;
use App\Modules\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Modules\Role\Models\Role;


class RoleController extends Controller{
    public function __construct(){
        # parent::__construct();
    }
    public function index(){
        $data = [];
        $role = Role::select('id', 'name')->get();
        $data['roles'] = $role;
        return view('Role::index', $data);
    }

    public function getAdd() {
        $data = [];
        $routes = Permission::orderBy('id', 'asc')->get();
        $data['routes'] = $routes;
        return view('Role::add', $data);
    }
    public function postAdd(Request $request) {
        $this->validate($request, [
            'nameRole'  => 'required',
        ]);
        $string_role = implode(',', $request->route);
        $role = new Role;
        $role->name = $request->nameRole;
        $role->value= $string_role;
        $role->save();
        return redirect()->route('Role.index')->with(['flash_message' => 'Add role successfully!', 'flash_level' => 'success']);
    }

    public function getDelete($id) {
        $user = DB::table('users')->where('role_id', $id)->first();
        if(!is_null($user)) {
            return back()->with(['flash_message' => 'User is using this role, change user\'s role first ', 'flash_level' => 'danger']);
        }
        $role = Role::find($id);
        if(!is_null($role)) {
            $role->delete();
            return redirect()->route('Role.index')->with(['flash_message' => 'Delete role successfully', 'flash_level' => 'success']);
        } else {
            return redirect()->route('Role.index')->with(['flash_message' => 'Role not found', 'flash_level' => 'danger']);
        }
    }

    public function getEdit($id) {
        $data = [];
        $routes = Permission::orderBy('id', 'asc')->get();
        $role = Role::find($id);
        $role_array = explode(',', $role->value);
        $data['routes'] = $routes;
        $data['role'] = $role;
        $data['role_array'] = $role_array;
        return view('Role::edit', $data);
    }

    public function postEdit(Request $request, $id) {
        $this->validate($request, [
            'nameRole'  => 'required'
        ]);
        $string_role = implode(',', $request->route);
        $role = Role::find($id);
        $role->name = $request->nameRole;
        $role->value = $string_role;
        $role->save();
        return redirect()->route('Role.index')->with(['flash_message' => 'Update role successfully', 'flash_level' => 'success']);
    }

   
} 