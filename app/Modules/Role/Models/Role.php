<?php

namespace App\Modules\Role\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name', 'value'];
    //public $timestamps = false;

}
