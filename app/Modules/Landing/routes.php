<?php

$prefix = "";  // URL prefix
// take module name
$module = basename(__DIR__);
// \\ make \ equal \ and it doesn't escape {} because {$} is prior
$namespace = "App\Modules\\{$module}\Controllers";

Route::group(
    ["prefix" => $prefix, "module" => $module , "namespace" => $namespace],
    function() use($module){
        Route::get('/', [
            # middle here
            "as" => "{$module}.index",
            "uses" => "{$module}Controller@index"
        ]);
    }
);
