<?php

namespace App\Modules\Landing\Models;

use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    public static function list(){
	    $result = [
	        ['id' => 1, 'title' => 'Category 1'],
	        ['id' => 2, 'title' => 'Category 2'],
	        ['id' => 3, 'title' => 'Category 3'],
	        ['id' => 4, 'title' => 'Category 4'],
	        ['id' => 5, 'title' => 'Category 5']
	    ];
	    return $result;
	}
}
