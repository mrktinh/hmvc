<?php

namespace App\Modules\Article\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	// default table, custome here..
    protected $table = 'articles';
    protected $fillable = ['name', 'slug', 'description', 'content', 'image'];
}
