<?php

$prefix = "article";  // URL prefix

$module = basename(__DIR__);
$namespace = "App\Modules\\{$module}\Controllers";

Route::group(
    ["prefix" => $prefix, "module" => $module , "namespace" => $namespace, 'middleware'=> ['authenticated', 'permission'] ],
    function() use($module){
        Route::get("/", [
            # middle here
            "as" => "{$module}.index",
            "uses" => "{$module}Controller@index"
        ]);
    	Route::post("add", [
    		"as" => "{$module}.postAdd", 
    		"uses" => "{$module}Controller@postAdd"
		]);
        Route::get("delete/{id}", [
            "as"    => "{$module}.getDelete",
            "uses"  => "{$module}Controller@getDelete"
        ]);
        Route::get('edit/{id}', [
            'as'    => "{$module}.getEdit", 
            'uses'  => "{$module}Controller@getEdit"
        ]);
        Route::post('edit/{id}', [
            'as'    => "{$module}.postEdit", 
            'uses'  => "{$module}Controller@postEdit"
        ]);
        // Route::post('delimg', [
        //     'as'    => "{$module}.getDelelteImage", 
        //     'uses'  => "{$module}Controller@postDelImg"
        // ]);

        Route::post('add-attach', [
            'as'    => "{$module}.postAddAttach", 
            'uses'  => "{$module}Controller@postAddAttach"
        ]);

        Route::post('delete-attach', [
            'as'    => "{$module}.postDeleteAttach", 
            'uses'  => "{$module}Controller@postDeleteAttach"
        ]);
    }
);