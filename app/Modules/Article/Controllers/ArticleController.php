<?php

namespace App\Modules\Article\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Article\Models\Article;
use App\Modules\Category\Models\Category;
use Illuminate\Support\Facades\Auth;
use File;
use DB;

class ArticleController extends Controller
{
    public function index() {
	     $data = [];
	     $listItem = Article::select('id', 'name', 'description', 'cate_id', 'created_at')->orderBy('id', 'DESC')->get();
         $cate = Category::select('id', 'name', 'parent_id')->get();
         $data['listItem'] = (!$listItem->isEmpty())? $listItem : null;
         $data['cate'] = $cate;
        return view('Article::index', $data);            
    }

    public function postAdd(Request $request) {
        $article = new Article;
        $article->name = $request->articleName;
        $article->cate_id = $request->category;
        $article->save();
        $id = $article->id;
        return redirect()->route('Article.getEdit', $id);
    }

    public function getDelete($id) {
        // delete file in content image
        $article= Article::find($id);

        // delete image in content
        // do not need to delete in database cause onDelete('cascade')
        $image_contents = DB::table('article_attaches')->where('article_id', $id)->get();
        foreach($image_contents as $content) {
            File::delete(base_path().'/public/upload/article/content/'.$content->image);
        }

        // delete file in detail image
        // do not need to delete in database cause onDelete('cascade')
        // $article_image_detail = Article::find($id)->images;
        //     foreach($article_image_detail as $item) {
        //         File::delete(base_path().'/public/upload/article/detail/'.$item->image);
        //     }
        // delete file in upload image
        File::delete(base_path().'/public/upload/article/'.$article->image);
        $article->delete();

        return redirect()->route('Article.index')->with(['flash_message' => 'Delete successfully!', 'flash_level' => 'info']);                   
    }

    public function getEdit($id) {
        $cate = Category::select('id', 'name', 'parent_id')->get()->toArray();
        $article = Article::find($id);
        return view('Article::edit', compact('cate', 'article'));
    }

    public function postEdit($id, Request $request) {
        $this->validate($request, [
            'category' => 'required',
            'articleName'   => 'required', 
            'content' => 'required',
        ]);
        $article = Article::find($id);
        $article->name = $request->articleName;
        $article->slug = str_slug($request->articleName);
        $article->description = $request->articleDescription;
        $article->content = $request->content;
        $article->user_id = Auth::user()->id;
        $article->cate_id = $request->category;

        $img_current = base_path().'/public/upload/article/'.$request->currentImgName;
        // click input current image
        if(!empty($request->file('image'))) {
            $file_name = getRandomName('image');
            $article->image= $file_name;
            $request->file('image')->move(base_path().'/public/upload/article/',$file_name);
            if(File::exists($img_current)) {
                File::delete($img_current);
            }
        } 
        $article->save();

        // click input detail image
        // if(!empty($request->file('fEditDetail'))) {
        //     foreach($request->file('fEditDetail') as $file){
        //         // echo '<pre>';
        //         // print_r($file);
        //         // echo '</pre>';

        //         $product_img = new ProductImage();
        //         if(!empty($file)) {
        //             $product_image_name = getRandomName($file);
        //             $product_img->image = $product_image_name;
        //             $product_img->product_id = $id;
        //             $file->move(base_path().'/public/upload/detail', $product_image_name);
        //             $product_img->save();
        //         }
        //     }
        // }
        return redirect()->route('Article.index')->with(['flash_message' => 'Update article successfully!', 'flash_level' => 'success']);
    }

    // public function postDelImg(Request $request) {
    //     if($request->ajax()) {
    //         $idHinh = (int)$request->idHinh;
    //         $image_detail = ProductImage::find($idHinh);
    //         if(!empty($image_detail)) {
    //             $img = base_path().'/public/upload/detail/'.$image_detail->image;
    //             if(File::exists($img)) {
    //                 File::delete($img);
    //             }
    //             $image_detail->delete();
    //         }
    //         return 'Okie';
    //     }
    // }

    public function postAddAttach(Request $request) {
        if($request->ajax()) {
            $path = base_path().'/public/upload/article/content/';
            $random_name = getRandomName('image');
            DB::table('article_attaches')->insert(['image' => $random_name, 'article_id' => $request->id]);
            if ($request->hasFile('image')) {
                $request->file('image')->move($path, $random_name);
            }
            return $random_name;
        }
    }

    public function postDeleteAttach(Request $request) {
        if($request->ajax()) {
            $name_of_file = basename($request->imageName);
            $image_path = public_path().'/upload/article/content/'.$name_of_file;
            DB::table('article_attaches')->where('image', $name_of_file)->delete();
            if(File::exists($image_path)) {
                File::delete($image_path);
            } else {
                return 'can not delete';
            }
            return $image_path;
            //File::delete('/opt/nginx/training/public/upload/content/05-frustrated-programmer-comic.png');
        }
    }


}
