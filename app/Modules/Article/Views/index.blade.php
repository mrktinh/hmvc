@extends('admin.master')
@section('controller', 'Article')
@section('action', 'list')
@section('content')
<form action="{{ route('Article.postAdd') }}" method="POST">
    <a href="#" data-toggle="modal" data-target="#addNewArticle" class="btn btn-primary" style="margin-left:40%" >Add new Article
    </a>
    <div id="addNewArticle" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert a new Article</h4>
                </div>
                <div class="form-group" style="padding: 20px 40px 0px 40px">
                    <label>Category Parent</label>
                    <select class="form-control" name="category">
                        <?php cate_parent($cate, 0, '**', old('category')); ?>
                    </select>
                </div>
                <div class="form-group" style="padding: 10px 40px 20px 40px">
                    <label>Article name</label>
                    <input type="text" name="articleName" value="" class="form-control" placeholder="Enter the name" />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success permission-edit"><span class="glyphicon glyphicon-off"></span> submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>


<table class="table table-striped table-bordered table-hover" id="listTableArticle">
    <thead>
        <tr align="center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Created</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <!-- <tfoot>
        <tr align="center">
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Date</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </tfoot> -->
    
    <tbody>
        @if(isset($listItem))
        @foreach($listItem as $index => $item)
        <tr class="odd gradeX" align="center">
            <td><a href="{{ url('article/edit', $item->id) }}">{{ $index + 1}}</a></td>
            <td><a href="{{ url('article/edit', $item->id) }}">{{ $item->name }}</a></td>
            <td><a href="{{ url('article/edit', $item->id) }}"> {{ ($item->description)? $item->description : 'No description' }}</a></td>
            <td><a href="{{ url('article/edit', $item->id) }}">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans() }}</a></td>
            <td class="center">
                <i class="fa fa-trash-o  fa-fw"></i>
                <a onclick="return confirm('Are you sure to delete {{ $item->name }}');" href="{{ url('article/delete', $item->id) }}"> Delete</a>
            </td>
            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{{ url('article/edit', $item->id) }}">Edit</a></td>
        </tr>
        @endforeach
        @endif
    </tbody>
    

</table>
                
@endsection