@extends('admin.master')
@section('controller', 'Article')
@section('action', 'edit')
@section('content')
<form action="#" method="POST" name="frmEditProduct" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="main-token" value="{!! csrf_token() !!}" />
    <input type="hidden" id="article_id" value="{{ $article->id }}" />
    <div class="col-lg-8 col-lg-offset-1" style="padding-bottom:120px">
        <div class="form-group">
            <label>Category Parent</label>
            <select class="form-control" name="category">
                <?php cate_parent($cate, 0, '**', $article->cate_id); ?>
            </select>
            @if($errors->has('category'))
                <p style="color:red">{{ $errors->first('category') }}</p>
            @endif
        </div>
        <div class="form-group">    
            <label>Name</label>
            <input class="form-control" name="articleName" value="{{ old('articleName', isset( $article->name )? $article->name: null ) }}" placeholder="Please Enter Username" />
            @if($errors->has('articleName'))
                <p style="color:red">{{ $errors->first('articleName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" rows="3" name="articleDescription" >{{ old('articleDescription', isset( $article->description )? $article->description: null ) }}</textarea>
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea class="form-control summernote" id="summernoteArticle" rows="3" name="content" >{{ old('txtContent', isset( $article->content )? $article->content: null ) }}</textarea>
            @if($errors->has('content'))
                <p style="color:red">{{ $errors->first('content') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Current Image</label>
            <img src="{{ (isset($article->image))? url('upload/article/'.$article->image): url('upload/No-Image-Basic.png') }}" alt="current_image" style="width:250px"/>
            <input type="hidden" name="currentImgName" value="{{ $article->image }}" />
        </div>
        <div class="form-group">
            <label>Change current Image</label>
            <input type="file" name="image" value="">
            @if($errors->has('image'))
                <p style="color:red">{{ $errors->first('image') }}</p>
            @endif
        </div>
        
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    </div>

</form>

@endsection