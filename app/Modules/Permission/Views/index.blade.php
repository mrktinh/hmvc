@extends('admin.master')
@section('controller', 'Permission')
@section('action', 'list')
@section('content')
<form action="{{ route('Permission.postSynchronize') }}" method="POST">
    <input type="submit" value="Synchronize" id="synchronize-button" class="btn btn-success" style="margin-left:40%"/>
    <input type="hidden" name="_token" id="permiss_token" value="{{ csrf_token() }}" />
</form>
<table class="table table-striped table-bordered table-hover" id="listTablePermission">
    <thead>
        <tr align="center">
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($routes as $index => $route)
            <tr>
                <td>
                    <a href="#" data-toggle="modal" data-target="#{{ str_replace('.', '',$route->name) }}">{{ $index + 1 }} 
                    </a>
                </td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#{{ str_replace('.', '',$route->name) }}">{{ $route->name }} </a>
                </td>
                <td id="permis-function{{ $route->id }}">
                    <a href="#" data-toggle="modal" data-target="#{{ str_replace('.', '',$route->name) }}">{{ $route->function }}</a>
                </td>
                <td class="center">
                    <a href="#" data-toggle="modal" data-target="#{{ str_replace('.', '',$route->name) }}">
                        <i class="fa fa-pencil fa-fw"></i> Edit
                    </a>
                </td>
            </tr>
            <div id="{{ str_replace('.', '',$route->name) }}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{ $route->name }}</h4>
                        </div>
                        <div class="form-group" style="padding: 20px 10px 20px 10px">
                            <input type="text" name="permissName" value="{{ $route->function }}" class="form-control" placeholder="Enter the Alias" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success permission-edit" data-dismiss="modal"><span class="glyphicon glyphicon-off"></span> submit</button>
                            <input type="hidden" value="{{ $route->id }}" />
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            
        @endforeach
    </tbody>
</table>



@endsection