<?php

namespace App\Modules\Permission\Controllers;
use App\Modules\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Route;
use DB;

class PermissionController extends Controller
{
    //
    public function index() {
    	$data = [];
    	$routes = Permission::orderBy('id', 'asc')->get();
    	$data ['routes'] = $routes;
    	return view('Permission::index', $data);

    	// $routes = Route::getRoutes();
    	// $data = [];
    	// foreach($routes as $route) {
    	// 	if(!empty($route->getPrefix()) && $route->getPrefix() != 'authentication') {
    	// 		$permission = new Permission;
	    // 		$permission->name = $route->getName();
	    // 		$permission->path = $route->getPath();
	    // 		$permission->slug = 'abc';
	    // 		$permission->function = 'none';
	    // 		$permission->save();	
    	// 	}
    	// }

    }

    public function postEdit(Request $request) {
    	$permiss = Permission::find($request->id);
        $permiss->function = $request->name;
        $permiss->save();
        return $request->name;
    }

    public function postSynchronize() {
    	// get all name route in db and delete
        $db_routes_name = Permission::all();
        $db_routes = [];
        foreach($db_routes_name as $route) {
            array_push($db_routes, $route);
        }
        DB::table('permissions')->delete();
		// get current routes and save into db, keep route in db if it existed.
    	$current_routes = Route::getRoutes();
    	foreach($current_routes as $route) {
            $temp = false;
    		if( !empty($route->getPrefix()) && $route->getPrefix() != 'authentication') {
	    		foreach($db_routes as $db_route) {
                    if( $route->getName() == $db_route->name) {
                        $permission = new Permission;
                        $permission->name = $db_route->name;
                        $permission->path = $route->getPath();
                        $permission->slug = $db_route->slug;
                        $permission->function = $db_route->function;
                        $permission->save();
                        
                        $temp = true;
                        break;
                    } 
                }
                if($temp == false) {
                    $permission = new Permission;
                    $permission->name = $route->getName();
                    $permission->path = $route->getPath();
                    $permission->slug = 'abc';
                    $permission->function = 'NOT SET';
                    $permission->save();
                }
                
    		}
    	}
    	return redirect()->route('Permission.index')->with(['flash_level' => 'success', 'flash_message' => 'Sychronize successfully!']);
    }

}


