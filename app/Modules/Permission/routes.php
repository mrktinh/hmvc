<?php

$prefix = "permission";  // URL prefix

$module = basename(__DIR__);
$namespace = "App\Modules\\{$module}\Controllers";

Route::group(
    ["prefix" => $prefix, "module" => $module , "namespace" => $namespace, 'middleware'=> 'authenticated'],
    function() use($module){
        Route::get("/", [
            # middle here
            "as" => "{$module}.index",
            "uses" => "{$module}Controller@index"
        ]);
        Route::post("edit", [
        	"as" => "{$module}.postEdit", 
        	"uses" => "{$module}Controller@postEdit"
      	]);
      	Route::post("synchronize", [
      		"as"	=> "{$module}.postSynchronize", 
      		"uses"	=> "{$module}Controller@postSynchronize"
  		  ]);

    }
); 