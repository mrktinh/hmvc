<?php

$prefix = "authentication";  // URL prefix

$module = basename(__DIR__);
$namespace = "App\Modules\\{$module}\Controllers";

Route::group(
    ["prefix" => $prefix, "module" => $module , "namespace" => $namespace,],
    function() use($module){
        Route::get("/", [
            # middle here
            "as" => "{$module}.login",
            "uses" => "{$module}Controller@login"
        ]);
        Route::post("login", [
        	"as" => "{$module}.postLogin", 
        	"uses" => "{$module}Controller@postLogin"
    	]);
    	Route::get("logout", [
    		"as" => "{$module}.getLogout", 
    		"uses" => "{$module}Controller@getLogout"
		]);

        Route::get("signup", [
            "as"    => "{$module}.getSignUp",
            "uses"  => "{$module}Controller@getSignUp"
        ]);
        Route::post('signup', [
            'as'    => "{$module}.postSignUp", 
            'uses'  => "{$module}Controller@postSignUp"
        ]);

        Route::get('activate/{token}', [
            'as'    => "{$module}.getActivate", 
            'uses'  => "{$module}Controller@getActivate"
        ]);
        Route::get('activate/password/{token}', [
            'as'    => "{$module}.getActivateChangePassword", 
            'uses'  => "{$module}Controller@getActivateChangePassword"
        ]);

        Route::get('password/reset', [
            'as'    => "{$module}.getPasswordReset", 
            'uses'  => "{$module}Controller@getPasswordReset"
        ]);
        Route::post('password/email', [
            'as'    => "{$module}.postPasswordEmail", 
            'uses'  => "{$module}Controller@postPasswordEmail"
        ]);

        Route::get('password/reset/{token}', [
            'as'    => "{$module}.getPasswordResetToken", 
            'uses'  => "{$module}Controller@getPasswordResetToken"
        ]);
        Route::post('password/reset', [
            'as'    => "{$module}.reset", 
            'uses'  => "{$module}Controller@reset"
        ]);
    }
);