<?php
namespace App\Modules\Authentication\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Hash;
use Mail;
use DB;


//use App\Modules\Authentication\Models\Category;


class AuthenticationController extends Controller{
    public function __construct(){
        # parent::__construct();
    }
    public function login(Request $request){
        
        return view('Authentication::login');
    }

    public function postLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email', 
            'password'  => 'required'
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            if(auth()->user()->verified == '0'){
                Auth::logout();
                return redirect()->route('Authentication.login')->with(['flash_level'=> 'warning','flash_message' => 'First please active your account']);
            } else{
                //return (Auth::user()->level == 2)? redirect()->route('Category.index') : redirect()->route('dashboard');
                return redirect()->route('Category.index');
            }
            
        } else {
            return redirect()->route('Authentication.login')->with(['flash_level'=> 'danger', 'flash_message' => 'Your email or password is not right!']);
        }
    }

    public function getLogout() {
        Auth::logout();
        return redirect()->route('Landing.index');
    }

    public function getSignUp() {
        return view('Authentication::signup');
    }

    public function postSignUp(Request $request) {
        $this->validate($request, [
            'name'   => 'required', 
            'email'  => 'required|email|unique:users,users.email', 
            'password' => 'required', 
            'password_confirmation' => 'required|same:password'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->remember_token = $request->_token;
        $user->save();

        $input = $request->all();
        // insert random token into authentication table to compare after
        $input['link'] = str_random(30);
        DB::table('authentications')->insert(['id_user'=>$user->id,'token'=> $input['link']]);
        // send email to verify
        Mail::send('Authentication::activation', $input, function($message) use ($input) {
            $message->to($input['email']);
            $message->subject('Site - Activation Code');
        });
        return redirect()->route('Authentication.login')->with(['flash_level'=> 'info', 'flash_message' => 'We sent activation code. Please check your mail.']);
        
    }
    // get token from email to compare with token in db
    public function getActivate($token) {
        if(Auth::check()) {
            Auth::logout();
        }
        $check = DB::table('authentications')->where('token',$token)->first();
        // update verified in user table
        if(!is_null($check)){
            $user = User::find($check->id_user);
            if($user->verified == true){
                return redirect()->route('Authentication.login')
                    ->with(['flash_level'=> 'success', 'flash_message' => 'You are already actived.']);
            }
            $user->verified = true;
            $user->save();
            DB::table('authentications')->where('token',$token)->delete();

            return redirect()->route('Authentication.login')
                ->with(['flash_level'=> 'success', 'flash_message' => 'You are already actived.']);
        }

        return redirect()->route('Authentication.login')
                ->with(['flash_level'=> 'danger', 'flash_message' => 'Your token is invalid.']);
        
    }
    // click email when is created by admin
    public function getActivateChangePassword($token) {
        if(Auth::check()) {
            Auth::logout();
        }
        $check = DB::table('authentications')->where('token',$token)->first();
        // update verified in user table
        if(!is_null($check)){
            $user = User::find($check->id_user);
            if($user->verified == true){
                return redirect()->route('Authentication.login')
                    ->with(['flash_level'=> 'success', 'flash_message' => 'You are already actived.']);
            }
            $user->verified = true;
            $user->save();
            DB::table('authentications')->where('token',$token)->delete();

            $input = [];
            $input['email'] = $user->email;
            $input['link'] = str_random(40);
            DB::table('password_resets')->insert(['email'=>$input['email'],'token'=> $input['link']]);

            return redirect()->route('Authentication.getPasswordResetToken',$input['link']);
        } else {
            return redirect()->route('Authentication.login')
                ->with(['flash_level'=> 'danger', 'flash_message' => 'Your token is invalid.']);
        }
        
    }

    // show forget password form
    public function getPasswordReset()
    {   
        if(Auth::check()) {
            Auth::logout();
        }
        return view('Authentication::email');
    }
    // process forgot password: send email
    public function postPasswordEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $input = [];
        $input['email'] = $request->email;
        $input['link'] = str_random(40);
        // delete email in password_reset table when user send next request to reset password
        DB::table('password_resets')->where('email', $input['email'])->delete();
        DB::table('password_resets')->insert(['email'=>$input['email'],'token'=> $input['link']]);
        // send email to verify
        Mail::send('Authentication::mailPassword', $input, function($message) use ($input) {
            $message->to($input['email']);
            $message->subject('Site - Activation Code');
        });
        return redirect()->route('Authentication.getPasswordReset')->with(['flash_level'=> 'info', 'flash_message' => 'We sent a link to reset your password']);
        
    }

    // Click button in email and Reset password form 
    public function getPasswordResetToken(Request $request, $token = null)
    {
        if(Auth::check()) {
            Auth::logout();
        }
        $check = DB::table('password_resets')->where('token', $token)->first();
        if(!is_null($check)) {
            return view('Authentication::reset')->with(
                ['token' => $token, 'email' => $request->email]
            );
        } else {
            return redirect()->route('Authentication.login')
                ->with(['flash_level'=> 'danger', 'flash_message' => 'Your token is invalid.']);
        }
        
    }

    public function reset(Request $request) {
        $check = DB::table('password_resets')->where('token', $request->token)->where('email', $request->email)->first();
        if(!is_null($check)) {
            $user = DB::table('users')->where('email', $check->email)->update(['password'=> bcrypt($request->password)]);
            DB::table('password_resets')->where('token', $request->token)->delete();
            return redirect()->route('Authentication.login')->with(['flash_level'=> 'success', 'flash_message'=> 'Change password successfully, you can login now!']);
        } else {
            return redirect()->back()->with(['flash_level'=> 'danger', 'flash_message' => 'Your token is invalid']);
        }
    }
    
    
   

} 