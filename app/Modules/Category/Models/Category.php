<?php

namespace App\Modules\Category\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'alias', 'order', 'parent_id', 'keywords', 'description'];
    //public $timestamps = false;

    public function product() {
    	return $this->hasMany('App\Modules\Models\Product');
    }

    public function category() {
    	if($this->parent_id){
    		return $this->find($this->parent_id);
    	}
    	return null;
    }
}
