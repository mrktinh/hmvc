<?php
namespace App\Modules\Category\Controllers;

use Validator;
use Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Category\Models\Category;


class CategoryController extends Controller{
    public function __construct(){
        # parent::__construct();
    }
    public function index(Request $request){
        $data = [];
        $data['listItem'] = Category::select('id', 'name', 'parent_id')->orderBy('id', 'DESC')->get();
        return view('Category::index', $data);
    }

    public function getAdd(Request $request) {
	    $parent = Category::select('id', 'name', 'parent_id')->get()->toArray();
	    return view('Category::add', compact('parent'));
    }

    public function postAdd(Request $request) {
    	$v = Validator::make($request->all(), [
	        'txtCateName' => 'required|unique:categories,name',
            'txtOrder'  => 'required|numeric',
    	], [
            'txtCateName.required'  => 'Please enter Name category', 
            'txtCateName.unique'    => 'This category name is existed',
            'txtOrder.required'    => 'This order is required',
            'txtOrder.numeric'    => 'This order must be digital'
    	]);
    	if ($v->fails()) {return redirect()->back()->withErrors($v->errors()); }
    	// $this->validate($request, [
	    //     'txtCateName' => 'required',
	    // ]);
        $cate = new Category;
        $cate->name = $request->txtCateName;
        $cate->slug = str_slug($request->txtCateName);
        $cate->order = $request->txtOrder;
        $cate->parent_id = $request->sltParent;
        $cate->keywords = $request->txtKeywords;
        $cate->description = $request->txtDescription;
        $cate->save();
        return redirect()->route('Category.index')->with(['flash_level'=> 'success','flash_message' => 'Add Category successfully!']);
    }

    public function getDelete($id) {
        $parent = Category::where('parent_id', $id)->count();
        if($parent > 0) {
            echo "<script> alert('Sorry! You can not delete this category');
                window.location = '";
            echo route('Category.index');

            echo "'; </script>";
        } else {
             $cate = Category::find($id)->delete();
            return redirect()->route('Category.index')->with(['flash_level' => 'info', 'flash_message'=>'Delete sussessfully!']);
        }
    }

    public function getEdit($id) {
        $data = Category::findOrFail($id)->toArray();
        $parent = Category::select('id', 'name', 'parent_id', 'order')->get()->toArray();
        return view('Category::edit', compact('parent', 'data'));
    }

    public function postEdit(Request $request, $id) {
        $v = Validator::make($request->all(), [
            'txtCateName' => 'required',
            'txtOrder'  => 'required',
        ], [
            'txtCateName.required'  => 'Please enter Name category', 
            'txtOrder.required'    => 'This order is required',
        ]);
        if ($v->fails()) {return redirect()->back()->withErrors($v->errors()); }
        $current_cate = Category::find($id);
        $cates = Category::all();
        $name_array = [];
        foreach($cates as $cate) {
            if($cate->name === $current_cate->name) {
                continue;
            }
            array_push($name_array, $cate->name);
        }
        if(in_array($request->txtCateName, $name_array)) {
            return back()->with(['flash_level' => 'danger', 'flash_message' => 'Category name is used, please choose another name!']);
        }
        $current_cate->name = $request->txtCateName;
        $current_cate->slug = str_slug($request->txtCateName);
        $current_cate->order = $request->txtOrder;
        $current_cate->parent_id = $request->sltParent;
        $current_cate->keywords = $request->txtKeywords;
        $current_cate->description = $request->txtDescription;
        $current_cate->save();
        return redirect()->route('Category.index')->with(['flash_level'=> 'success','flash_message' => 'Update successfully!']);
    }
} 