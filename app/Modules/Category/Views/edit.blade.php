@extends('admin.master')
@section('controller', 'Category')
@section('action', 'edit')
@section('content')

<!-- /.col-lg-12 -->
<div class="col-lg-7 col-lg-offset-3" style="padding-bottom:120px">
    {{--  @include('admin.blocks.error') --}}
    <form action="{!! route('Category.getEdit', $data['id']) !!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="form-group">
            <label>Category Parent</label>
            <select class="form-control" name="sltParent">
                <option value="0">Please Choose Category</option>
                <?php cate_parent($parent, 0,  "**", $data['parent_id']) ?>
            </select>
        </div>
        <div class="form-group">
            <label>Category Name</label>
            <input class="form-control" name="txtCateName" required="required" value="{!! old('txtCateName', isset($data)? $data['name']: null) !!}" placeholder="Please Enter Category Name" />
            @if($errors->has('txtCateName'))
            <p style="color:red">{{ $errors->first('txtCateName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Category Order</label>
            <input type="number" class="form-control" name="txtOrder" required="required" value="{!! old('txtOrder', isset($data['order'])? $data['order']: null) !!}" placeholder="Please Enter Category Order" />
            @if($errors->has('txtOrder'))
            <p style="color:red">{{ $errors->first('txtOrder') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Category Keywords</label>
            <input class="form-control" name="txtKeywords" value="{!! old('txtKeywords', isset($data)? $data['keywords']: null) !!}" placeholder="Please Enter Category Keywords" />
            @if($errors->has('txtKeywords'))
            <p style="color:red">{{ $errors->first('txtKeywords') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Category Description</label>
            <textarea class="form-control" rows="3" name="txtDescription" value="{!! old('txtDescription', isset($data)? $data['description']: null) !!}"></textarea>
            @if($errors->has('txtDescription'))
            <p style="color:red">{{ $errors->first('txtDescription') }}</p>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">Category Edit</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    <form>
</div>
@endsection     
