@extends('admin.master')
@section('controller', 'Category')
@section('action', 'List')
@section('content')

<!-- Page Content -->

<a href="{{ route('Category.getAdd') }}" class="btn btn-primary" style="margin-left:40%" >Add new Category</a>
<table class="table table-striped table-bordered table-hover" id="listTableCategory">
    <thead>
        <tr align="center">
            <th>ID</th>
            <th>Name</th>
            <th>Category Parent</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>

        @foreach($listItem as $index => $item)
            <tr class="odd gradeX" align="center">
                <td><a href="{{ route('Category.getEdit', $item->id) }}">{{ $index + 1 }}</a></td>
                <td><a href="{{ route('Category.getEdit', $item->id) }}">{{ $item->name }}</a></td>
                <td>
                    @if(!$item->parent_id) 
                        <a href="{{ route('Category.getEdit', $item->id) }}">Root</a>
                    @else
                        <a href="{{ route('Category.getEdit', $item->id) }}">{{ $item->category()->name }}</a>
                    @endif
                    
                </td>
                <td class="center">
                    <i class="fa fa-trash-o  fa-fw"></i>
                    <a 
                        onclick=" 
                        return xacnhanxoa(' Bạn có chắc muốn xóa {{ $item->name }} không?');" 
                        href="{{ url('category/delete', $item->id) }}">
                         Delete
                    </a>
                </td>
                <td class="center">
                    <i class="fa fa-pencil fa-fw"></i> 
                    <a href="{{ url('category/edit', $item->id) }}">
                        Edit
                    </a>
                    </td>
            </tr>
        @endforeach
    </tbody>
</table>
        
@endsection

    