@extends('admin.master')
@section('controller', 'User')
@section('action', 'List')
@section('content')
<a href="{{ route('User.getAdd') }}" class="btn btn-primary" style="margin-left:40%" >Add new User</a>
<table class="table table-striped table-bordered table-hover" id="userTableList">
    <thead>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Role</th>
            <th>Verified</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        @foreach($listUser as $index => $item)
        <tr class="odd gradeX" align="center">
            <td><a href="{{ route('User.getEdit', $item->id) }}">{{ $index+1 }}</a></td>
            <td><a href="{{ route('User.getEdit', $item->id) }}">{{ $item->name }}</a></td>
            <td><a href="{{ route('User.getEdit', $item->id) }}">{{ $item->email }}</a></td>
            {{-- <td><a href="{{ route('User.getEdit', $item->id) }}">{{ $item->role()->first()->name }}</a></td> --}}
            <td><a href="{{ route('User.getEdit', $item->id) }}">
                @if($item->verified)
                    <i class="fa fa-check" aria-hidden="true"></i>
                @else 
                    <i class="fa fa-times" aria-hidden="true"></i>
                @endif
            </a></td>
            <td ><i class="fa fa-trash-o  fa-fw"></i><a onclick="return confirm('Are you sure to delete {{ $item->name }}')" href="{{ url('user/delete', $item->id) }}"> Delete</a></td>
            <td ><i class="fa fa-pencil fa-fw"></i> <a href="{{ url('user/edit', $item->id) }}">Edit</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
                
@endsection