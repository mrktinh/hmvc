@extends('admin.master')
@section('controller', 'User')
@section('action', 'Add')
@section('content')
<div class="col-lg-6 col-lg-offset-3" style="padding-bottom:120px">
    <form action="{{ route('User.getEdit', $user->id) }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
            <label>User Role</label>
            <select class="form-control" name="role">
                @foreach( $roles as $role)
                <option value="{{ $role->id }}"
                    @if ($role->id == $user->role_id)
                        selected="selected"
                    @endif
                >{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input class="form-control" name="userName" placeholder="Please Enter Username" value="{{ old('userName', isset( $user->name )? $user->name: null ) }}"/>
            @if($errors->has('userName'))
            <p style="color:red">{{ $errors->first('userName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" placeholder="Please Enter Email" value="{{ old('email', isset( $user->email )? $user->email: null ) }}"/>
            @if($errors->has('email'))
            <p style="color:red">{{ $errors->first('email') }}</p>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    <form>
</div>

@endsection
                
