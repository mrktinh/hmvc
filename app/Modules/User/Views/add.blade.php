@extends('admin.master')
@section('controller', 'User')
@section('action', 'Add')
@section('content')
<div class="col-lg-5 col-lg-offset-3 " style="padding-bottom:120px">
    <form action="{{ url('user/add') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
            <label>User Role</label>
            <select class="form-control" name="role">
                @foreach( $roles as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input class="form-control" name="userName" placeholder="Please Enter Username" value="{{ old('userName') }}"/>
            @if($errors->has('userName'))
            <p style="color:red">{{ $errors->first('userName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" placeholder="Please Enter Email" value="{{ old('email') }}"/>
            @if($errors->has('email'))
            <p style="color:red">{{ $errors->first('email') }}</p>
            @endif
        </div>
        
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    <form>
</div>

@endsection
                
