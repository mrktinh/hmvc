<?php

namespace App\Modules\User\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Hash;
use Mail;
use DB;

class UserController extends Controller
{
    public function index() {
        $user = [];
        $user['listUser'] = User::select('id','name', 'email', 'role_id', 'verified')->orderBy('id', 'DESC')->get();
        return view('User::index', $user);
    }

    public function getAdd() {
        $data = [];
        $role = DB::table('roles')->select('id','name')->get();
        $data['roles'] = $role;
        return view('User::add', $data);
    }

    public function postAdd(Request $request) {
        $this->validate($request, [
            'userName' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);
    	$user = new User();
    	$user->name = $request->userName;
    	$user->password = Hash::make('x');
    	$user->email = $request->email;
        $user->role_id = $request->role;
    	$user->remember_token = $request->_token;
    	$user->save();

        $input = $request->all();
        // insert random token into authentication table to compare after
        $input['link'] = str_random(30);
        DB::table('authentications')->insert(['id_user'=>$user->id,'token'=> $input['link']]);
        
        Mail::send('User::activation', $input, function($message) use ($input) {
            $message->to($input['email']);
            $message->subject('Site - Activation Code');
        });

    	return redirect()->route('User.index')->with(['flash_message' => 'Add user successfully', 'flash_level' => 'success']);
    }

    public function getDelete($id) {
        $user_login_level = Auth::user()->level;
        $user = User::find($id);
        // ko xoa neu la admin va superadmin
        if( $user_login_level == 3 && $user->level < 3) {
            $user->delete();
            return redirect()->route('User.index')->with(['flash_message' => 'Delete user successfully', 'flash_level' => 'success']);
        } else if( $user_login_level == 2 && $user->level < 2 ) {
            $user->delete();
            return redirect()->route('User.index')->with(['flash_message' => 'Delete user successfull', 'flash_level' => 'success']); 
        } else {
            return back()->with(['flash_message' => 'You can not delete this user', 'flash_level' => 'danger']);
        }
    }

    public function getEdit($id) {
        $data = [];
        $user = User::find($id);
        $data['user'] = $user;
        $role = DB::table('roles')->select('id', 'name')->get();
        $data['roles'] = $role;
        return view('User::edit', $data);
    }

    public function postEdit($id, Request $request){
        $this->validate($request, 
        [
            'userName' => 'required',
            'email' => 'required|email'
        ]);
        $current_user = User::find($id);
        // get all other user's email
        $users = User::all();
        $email_array = [];
        foreach($users as $user) {
            if($user->email == $current_user->email) {
                continue;
            } else {
                array_push($email_array, $user->email);
            }
        }
        if(in_array($request->email, $email_array)) {
            return back()->with(['flash_message' => 'Email is used, please choose another email', 'flash_level' => 'danger']);
        }
        $current_user->name = $request->userName;
        $current_user->email = $request->email;
        $current_user->role_id = $request->role;
        $current_user->remember_token = $request->_token;
        $current_user->save();
        return redirect()->route('User.index')->with(['flash_message' => 'Update user successfully', 'flash_level' => 'success']); 
    }
}
