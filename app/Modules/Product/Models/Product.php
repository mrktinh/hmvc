<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'alias', 'price', 'intro', 'content', 'image', 'keywords', 'description', 'user_id', 'cate_id'];
    //public $timestamps = false;

    public function category() {
    	return $this->belongsTo('App\Modules\Category\Models\Category', 'cate_id');
    }

    public function user() {
    	return $this->belongsTo('App\Modules\User\Models\User', 'user_id');
    }

    public function images() {
    	return $this->hasMany('App\Modules\ProductImage\Models\ProductImage', 'product_id');
    }

    public function attachs() {
        return $this->hasMany('App\Modules\Attach\Models\Attach');
    }

    // public function categoryName() {
    //     if($this->cate_id) {
    //         return Cate::find($this->cate_id);
    //     }
    // }
}
