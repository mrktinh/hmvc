@extends('admin.master')
@section('controller', 'Product')
@section('action', 'add')
@section('content')
<form action="{!! url('product/add') !!}" method="POST" enctype="multipart/form-data">

    <div class="col-lg-7" style="padding-bottom:120px">
        <input type="hidden" name="_token" id="main-token" value="{!! csrf_token() !!}" />
        <div class="form-group">
            <label>Category Parent</label>
            <select class="form-control" name="category">
                <option value="">Please Choose Category</option>
                <?php cate_parent($cate, 0, '**', old('category')); ?>
            </select>
            @if($errors->has('category'))
                <p style="color:red">{{ $errors->first('category') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Name</label>
            <input class="form-control" name="productName" value="{!! old('productName', isset($productName)? $productName : null) !!}" placeholder="Please enter the product name" />
            @if($errors->has('productName'))
                <p style="color:red">{{ $errors->first('productName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Price</label>
            <input class="form-control" name="price" type="number" value="{!! old('price') !!}" placeholder="Please Enter Price" />
            @if($errors->has('price'))
                <p style="color:red">{{ $errors->first('price') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Intro</label>
            <textarea   class="form-control" rows="3" name="txtIntro" >{{ old('txtIntro') }}</textarea>
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea class="form-control summernote" rows="3" name="txtContent" >{{ old('txtContent') }}</textarea>
        </div>
        <div class="form-group">
            <label>Images</label>
            <input type="file" name="image" value="{!! old('image') !!}">
            @if($errors->has('image'))
                <p style="color:red">{{ $errors->first('image') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Product Keywords</label>
            <input class="form-control" name="txtKeywords" value="{!! old('txtKeywords') !!}" placeholder="Please Enter Category Keywords" />
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    </div>

    <div class="col-md-1"></div>
    <div class="col-md-4">
        @for($i = 1; $i<= 5; $i++)
        <div class="form-group">
            <label>Image Product Detail {{ $i }}</label>
            <input type="file" name="fProductDetail[]" />
        </div>
        @endfor
    </div>
</form>

@endsection