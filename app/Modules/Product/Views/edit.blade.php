@extends('admin.master')
@section('controller', 'Product')
@section('action', 'edit')
@section('content')
<form action="{{ route('Product.postEdit', $product->id) }}" method="POST" name="frmEditProduct" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="main-token" value="{!! csrf_token() !!}" />
    <input type="hidden" id="product_id" value="{{ $product->id }}" />
    <div class="col-lg-7" style="padding-bottom:120px">
        <div class="form-group">
            <label>Category Parent</label>
            <select class="form-control" name="category">
                <option value="">Please Choose Category</option>
                <?php cate_parent($cate, 0, '**', $product->cate_id); ?>
            </select>
            @if($errors->has('category'))
                <p style="color:red">{{ $errors->first('category') }}</p>
            @endif
        </div>
        <div class="form-group">    
            <label>Name</label>
            <input class="form-control" name="productName" value="{{ old('productName', isset( $product->name )? $product->name: null ) }}" placeholder="Please Enter Username" />
            @if($errors->has('productName'))
                <p style="color:red">{{ $errors->first('productName') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="number" class="form-control" name="price" value="{{ old('price', isset( $product->price )? $product->price: null ) }}" placeholder="Please Enter Price" />
            @if($errors->has('price'))
                <p style="color:red">{{ $errors->first('price') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label>Intro</label>
            <textarea class="form-control" rows="3" name="txtIntro" >{{ old('txtIntro', isset( $product->intro )? $product->intro: null ) }}</textarea>
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea class="form-control summernote" id="summernoteProduct" rows="3" name="txtContent" >{{ old('txtContent', isset( $product->content )? $product->content: null ) }}</textarea>
        </div>
        <div class="form-group">
            <label>Current Image</label>
            <img src="{{ (isset($product->image))? url('upload/product/'.$product->image): url('upload/No-Image-Basic.png') }}" alt="current_image" style="width:250px"/>
            <input type="hidden" name="currentImgName" value="{{ $product->image }}" />
        </div>
        <div class="form-group">
            <label>Change current Image</label>
            <input type="file" name="fImages" value="">
        </div>
        <div class="form-group">
            <label>Product Keywords</label>
            <input class="form-control" name="txtKeywords" value="{{ old('txtPrice', isset( $product->price )? $product->name: null ) }}" placeholder="Please Enter Category Keywords" />
        </div>
        <div class="form-group">
            <label>Product Description</label>
            <textarea class="form-control" rows="3" name="txtProDes" value="{{ old('txtKeywords', isset( $product->description )? $product->description: null ) }}"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    </div>

    <div class="col-md-1"></div>
    <div class="col-md-4">
        @foreach ($product_image as $itemImage)
        <div class="form-group" id="{{ $itemImage['id'] }}" style="margin-bottom:30px">
            <img src="{{ url('upload/product/detail/'.$itemImage['image']) }}" idHinh="{{ $itemImage['id'] }}"
             alt="detail_image" style="width:80%;height:180px"/>
            <a href="javascript:void(0)" id="del_img_demo" class="btn btn-danger btn-circle icon-del" style="position:relative;top:-75px;left:-20px"><i class="fa fa-times"></i>
            </a>
        </div>
        @endforeach
        <div type="button" class="btn btn-primary" id="addImages">Add Images</div>
        <div id="insertMoreImage" style="margin-top: 10px"></div>
    </div>
</form>

@endsection