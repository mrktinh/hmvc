<?php

namespace App\Modules\Product\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Product\Models\Product;
use App\Modules\Category\Models\Category;
use App\Modules\ProductImage\Models\ProductImage;
use Illuminate\Support\Facades\Auth;
use File;
use DB;

class ProductController extends Controller
{
    public function index() {
	    $data = [];
	    $listItem = Product::select('id', 'name', 'price', 'cate_id', 'created_at')->orderBy('id', 'DESC')->get();
        $cate = Category::select('id', 'name', 'parent_id')->get();
        $data['listItem'] = (!$listItem->isEmpty())? $listItem : null;
        $data['cate'] = $cate;
        return view('Product::index', $data);            
    }

    public function postAdd(Request $request) {
        $product = new Product;
        $product->name = $request->productName;
        $product->cate_id = $request->category;
        $product->save();
        $id = $product->id;
        return redirect()->route('Product.getEdit', $id);

    }

    public function getDelete($id) {
        // delete file in content image
        $product= Product::find($id);

        // delete image in content
        $image_contents = DB::table('attaches')->where('product_id', $id)->get();
        foreach($image_contents as $content) {
            File::delete(base_path().'/public/upload/product/content/'.$content->image);
        }

        // delete file in detail image
        // do not need to delete in database cause onDelete('cascade')
        $product_image_detail = Product::find($id)->images;
            foreach($product_image_detail as $item) {
                File::delete(base_path().'/public/upload/product/detail/'.$item->image);
            }
        // delete file in upload image
        File::delete(base_path().'/public/upload/product/'.$product->image);
        $product->delete();

        return redirect()->route('Product.index')->with(['flash_message' => 'Delete successfully!', 'flash_level' => 'info']);                   
    }

    public function getEdit($id) {
        $cate = Category::select('id', 'name', 'parent_id')->get()->toArray();
        $product = Product::find($id);
        $product_image = Product::find($id)->images;
        return view('Product::edit', compact('cate', 'product', 'product_image'));
    }

    public function postEdit($id, Request $request) {
        $this->validate($request, [
            'category' => 'required',
            'productName'   => 'required', 
            'price' => 'bail|required|integer',
        ]);
        $product = Product::find($id);
        $product->name = $request->productName;
        $product->slug = str_slug($request->productName);
        $product->price = $request->price;
        $product->intro = $request->txtIntro;
        $product->content = $request->txtContent;
        $product->keywords = $request->txtKeywords;
        $product->user_id = Auth::user()->id;
        $product->cate_id = $request->category;

        $img_current = base_path().'/public/upload/product/'.$request->currentImgName;
        // click input current image
        if(!empty($request->file('fImages'))) {
            $file_name = getRandomName('fImages');
            $product->image= $file_name;
            $request->file('fImages')->move(base_path().'/public/upload/product/',$file_name);
            if(File::exists($img_current)) {
                File::delete($img_current);
            }
        } 
        $product->save();

        // click input detail image
        if(!empty($request->file('fEditDetail'))) {
            foreach($request->file('fEditDetail') as $file){
                // echo '<pre>';
                // print_r($file);
                // echo '</pre>';

                $product_img = new ProductImage();
                if(!empty($file)) {
                    $product_image_name = getRandomName($file);
                    $product_img->image = $product_image_name;
                    $product_img->product_id = $id;
                    $file->move(base_path().'/public/upload/product/detail/', $product_image_name);
                    $product_img->save();
                }
            }
            
        }
        return redirect()->route('Product.index')->with(['flash_message' => 'Update product successfully!', 'flash_level' => 'success']);
    }

    public function postDelImg(Request $request) {
        if($request->ajax()) {
            $idHinh = (int)$request->idHinh;
            $image_detail = ProductImage::find($idHinh);
            if(!empty($image_detail)) {
                $img = base_path().'/public/upload/product/detail/'.$image_detail->image;
                if(File::exists($img)) {
                    File::delete($img);
                }
                $image_detail->delete();
            }
            return 'Okie';
        }
    }

    public function postAddAttach(Request $request) {
        if($request->ajax()) {
            $path = base_path().'/public/upload/product/content/';
            $random_name = getRandomName('image');
            DB::table('attaches')->insert(['image' => $random_name, 'product_id' => $request->id]);
            if ($request->hasFile('image')) {
                $request->file('image')->move($path, $random_name);
            }
            return $random_name;
        }
    }

    public function postDeleteAttach(Request $request) {
        if($request->ajax()) {
            $name_of_file = basename($request->imageName);
            $image_path = public_path().'/upload/product/content/'.$name_of_file;
            DB::table('attaches')->where('image', $name_of_file)->delete();
            if(File::exists($image_path)) {
                File::delete($image_path);
            } else {
                return 'can not delete';
            }
            return $image_path;
            //File::delete('/opt/nginx/training/public/upload/content/05-frustrated-programmer-comic.png');
        }
    }


}
