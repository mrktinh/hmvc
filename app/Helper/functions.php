<?php 

function stripUnicode($str){
	if(!$str) return false;
		$unicode = array(
			'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
			'd'=>'đ',
			'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
			'i'=>'í|ì|ỉ|ĩ|ị',
			'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
			'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
			'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		);
	foreach($unicode as $nonUnicode=>$uni) 
		$str = preg_replace("/($uni)/i",$nonUnicode,$str);
	return $str;
}

function changeTitle($title) {
	$str = preg_replace('/[ ]+/', '-', trim(stripUnicode($title)));
	return $str;
}

function cate_parent($data, $parent=0, $str="**", $select=0) {
	foreach($data as $val) {
		$id = $val['id'];
		$name= $val['name'];
		if($val['parent_id'] == $parent) {
			if($select !=0 && $id == $select){
				echo "<option value='$id' selected='selected'>$str $name</option>";
			} else {
				echo "<option value='$id'>$str $name</option>";
			}	
			cate_parent($data, $id, "----", $select);
		}
	}
}

function getRandomName($input) {
	$extension = is_object($input)? $input->extension() : Request::file($input)->extension();
    $random_str = str_random(32);
    return $random_str.'.'.$extension;
}

function checkAdmin() {
	if(Auth::user()->level > 1) {
	    return true;
	} else {
	    return back()->with(['flash_level'=> 'warning','flash_message' => 'Can not access this area!']);
	}
}


