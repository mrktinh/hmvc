// Page-Level Demo Scripts - Tables - Use for reference
$(document).ready(function() {
        $('#listTableProduct').DataTable({
                responsive: true
        });

        $('#listTableCategory').DataTable({
                responsive: true
        });
        $('#userTableList').DataTable({
        	responsive: true
        });
        $('#listTablePermission').DataTable({
        	responsive: true, 
        	"lengthMenu": [[25, 50, -1], [25, 50, "All"]], 
        	"order": [[0, "asc"]]
        });
        $('#RoleListTable').DataTable({
        	responsive: true,
        });
        $('#RoleGetList').DataTable({
        	responsive: true,
        });
        $('#listTableArticle').DataTable({
        	responsive: true,
        });
        
});


// Summer note in editing product
$(document).ready(function() {
	var base_url = window.location.protocol + "//" + window.location.host;
	var IMAGE_PATH =  base_url + '/public/upload/product/content/';
	$('#summernoteProduct').summernote({
		height: 400, 
		callbacks: {
			onImageUpload: function(files) {
		        data = new FormData();
		        data.append("image", files[0]);
		        data.append('id', $("#product_id").val());
		        data.append('_token', $("#main-token").val());
		        $.ajax({
		            data: data,
		            type: "POST",
		            url: "/product/add-attach",
		            cache: false, 
		            contentType: false,
		            processData: false,
		            success: function(filename) {
		                var file_path = IMAGE_PATH + filename;
		                $('.summernote').summernote("insertImage", file_path);
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
				        alert(xhr.status);
				        alert(thrownError);
			        }
		        });
	    	},

		 	onMediaDelete : function($target, editor, $editable) {
		 		data = new FormData();
		 		data.append('imageName', $target[0].src);
		 		// imageName is pass is full url of image
		 		data.append('_token', $("#main-token").val());
		 		$.ajax({
		 			data: data, 
		 			type: "POST", 
		 			url: "/product/delete-attach", 
		 			cache: false, 
		 			contentType: false, 
		 			processData: false, 
		 			success: function(name) {
		 				$target.remove();
		 			}, 
		 			error: function (xhr, ajaxOptions, thrownError) {
				        alert(xhr.status);
				        alert(thrownError);
			        }

		 		});	

		    }	    	
		}
			
	});

});
// summernote in editing article
$(document).ready(function() {
	var base_url = window.location.protocol + "//" + window.location.host;
	var IMAGE_PATH =  base_url + '/public/upload/article/content/';
	$('#summernoteArticle').summernote({
		height: 400, 
		callbacks: {
			onImageUpload: function(files) {
		        data = new FormData();
		        data.append("image", files[0]);
		        data.append('id', $("#article_id").val());
		        data.append('_token', $("#main-token").val());
		        $.ajax({
		            data: data,
		            type: "POST",
		            url: "/article/add-attach",
		            cache: false, 
		            contentType: false,
		            processData: false,
		            success: function(filename) {
		                var file_path = IMAGE_PATH + filename;
		                $('.summernote').summernote("insertImage", file_path);
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
				        alert(xhr.status);
				        alert(thrownError);
			        }
		        });
	    	},

		 	onMediaDelete : function($target, editor, $editable) {
		 		data = new FormData();
		 		data.append('imageName', $target[0].src);
		 		// imageName is pass is full url of image
		 		data.append('_token', $("#main-token").val());
		 		$.ajax({
		 			data: data, 
		 			type: "POST", 
		 			url: "/article/delete-attach", 
		 			cache: false, 
		 			contentType: false, 
		 			processData: false, 
		 			success: function(name) {
		 				$target.remove();
		 			}, 
		 			error: function (xhr, ajaxOptions, thrownError) {
				        alert(xhr.status);
				        alert(thrownError);
			        }

		 		});	

		    }	    	
		}
			
	});

});

// message
$('div.alert').delay(4000).slideUp();

$(document).ready(function(){
	 xacnhanxoa =  function(msg) {
		if(window.confirm(msg)) {
			return true;
		} 
		return false;
	}
});
// click add image
$(document).ready(function(){
	$('div#addImages').click(function() {
		$('div#insertMoreImage').append('<div class="form-group"><input type="file" name="fEditDetail[]"/></div>');
	});
});

// ajax del image on red button
$(document).ready(function() {
	$('a#del_img_demo').on('click', function() {
		//var _token = $("form[name='frmEditProduct']").find("input[name='_token']").val();
		var idHinh = $(this).parent().find("img").attr("idHinh");
		//var pathHinh = $(this).parent().find("img").attr("src");
		data = new FormData();
		data.append('idHinh', idHinh);
		data.append('_token', $("#main-token").val());
		$.ajax({
			url: '/product/delimg', 
			data: data,
			type: 'POST', 
			cache: false, 
			contentType: false, 
 			processData: false,
			success: function(data){
				if(data == "Okie") {
					$("#"+idHinh).remove();
				} else {
					alert("Error! Please contact Admin");
				}
				//console.log(data);
			}
		});
	});
});

// tick all box at roles.
$(document).ready(function() {
	 $("#checkAll").click(function () {
        if ($("#checkAll").is(':checked')) {
            $(".radioR").prop("checked", true);
        } else {
            $(".radioR").prop("checked", false);
        }
    });
})

//click button submit in permission edit
$(document).ready(function() {
	$('button.permission-edit').on('click', function() {
		var id = $(this).parent().find("input").attr("value");
		var name = $(this).parent().parent().find("input[name='permissName']").val();
		var data = new FormData();
		data.append('id', id);
		data.append('name', name);
		data.append('_token', $('#permiss_token').val());
		$.ajax({
			url: 'permission/edit', 
			data: data, 
			type: 'POST', 
			cache: false, 
			contentType: false, 
			processData: false, 
			success: function(data) {
				if(data == name) {
					$('#permis-function'+id).html(data);
				}
			}
		});
	});
});
	