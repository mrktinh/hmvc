<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductSaveNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ( $table) {
            $table->string('name')->nullable()->change();
            $table->dropColumn('alias');
            $table->string('slug')->nullable();
            $table->integer('price')->nullable()->change();
            $table->text('intro')->nullable()->change();
            $table->longText('content')->nullable()->change();
            $table->string('image')->nullable()->change();
            $table->string('keywords')->nullable()->change();
            $table->integer('user_id')->unsigned()->nullable()->change();
            $table->integer('cate_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table) {

        });
    }
}
