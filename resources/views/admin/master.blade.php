<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Khóa Học Lập Trình Laravel Framework 5.x Tại Khoa Phạm">
    <meta name="author" content="Vu Quoc Tuan">
    <title>Admin - Management</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    
    <!-- MetisMenu CSS -->
    <link href="{{ asset('/admin/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/admin/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{{ asset('/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    
    <!-- Summernote css -->
    <link rel="stylesheet" href="{{ asset('/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('/admin/css/style.css') }}">
    <!-- DataTables Responsive CSS -->
    <!-- <link href="{{ asset('/admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet"> -->


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Admin Area </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        <li><a href="{{ url('authentication/password/reset') }}"><i class="fa fa-gear fa-fw"></i> Change password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ route('Authentication.getLogout') }}">
                                <i class="fa fa-sign-out fa-fw"></i>Logout
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <!-- <li>
                            <a href="{{ url('/home') }}"><i class="fa fa-dashboard fa-fw"></i>&nbsp; Dashboard</a>
                        </li> -->
                        <li>
                            <a href="{{ route('Category.index') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Category</a>
                            <!-- <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('Category.index') }}">List Category</a>
                                </li>
                                <li>
                                    <a href="{{ route('Category.getAdd') }}">Add Category</a>
                                </li>
                            </ul> -->
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('Product.index') }}"><i class="fa fa-cube fa-fw"></i>&nbsp; Product</a>
                            {{-- 
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ route('Product.index') }}">List Product</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('Product.getAdd') }}">Add Product</a>
                                    </li>
                                </ul>
                            --}}
                            
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('Article.index') }}"><i class="fa fa-newspaper-o fa-fw"></i>&nbsp; Article</a>
                        </li>
                        <li>
                            <a href="{{ route('User.index') }}"><i class="fa fa-users fa-fw"></i>&nbsp; User</a>
                            <!-- <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('User.index') }}">List User</a>
                                </li>
                                <li>
                                    <a href="{{ route('User.getAdd') }}">Add User</a>
                                </li>
                            </ul> -->
                            <!-- /.nav-second-level -->
                        </li>
                        <!-- Member will not see Role-->
                        <li>
                            <a href="{{ route('Permission.index') }}"><i class="fa fa-wrench fa-fw"></i>&nbsp; Permission</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('Role.index') }}"><i class="fa fa-fire fa-fw"></i>&nbsp; Role</a>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('controller')
                            <small>@yield('action')</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12">
                    @if(session('flash_message'))
                        <div class="alert alert-{!! session('flash_level') !!}" style="padding:10px;text-align:center;">
                            {!! session('flash_message') !!}
                        </div>
                    @endif
                    </div>
                    <!-- Đây là nơi chứa nội dung -->
                    @yield('content')
                    <!-- End Đây là nơi chứa nội dung -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('/admin/bower_components/jquery/dist/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('/admin/bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('/admin/dist/js/sb-admin-2.js')}}"></script>

    <!-- DataTables JavaScript -->
    <script src="{{ asset('/admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

    <!-- Summernote js -->
    <script src="{{ asset('/bower_components/summernote/dist/summernote.js')}}"></script>

    <!-- Custom JavaScript -->
    <script src="{{ asset('/admin/js/myscript.js')}}"></script>

    
</body>

</html>
